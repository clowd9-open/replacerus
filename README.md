# replacerus

It's a fork of https://github.com/whuang8/redactrus with small changes.

Example with logrusField as Json type

```go
package main

import (
  log "github.com/sirupsen/logrus"
  "gitlab.com/clowd9-open/replacerus"
)

func init() {
  // Create Redactrus hook that is triggered
  // for every logger level and redacts any
  // Logrus fields with the key as 'password'
  rh := &replacerus.Hook{
      AcceptedLevels: log.AllLevels,
      ReplaceList: []string{"(\"other\":\")\\S+(\")"},
  }
  
  log.AddHook(rh)
}

func main() {
  log.WithFields(log.Fields{
    "walrusName": "Walrein",
    "frame": "iloveclams<3",
  }).Info("A walrus attempted to log in.")
}
```