package replacerus

import (
	"fmt"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

var h = &Hook{}

type levelsTest struct {
	name           string
	acceptedLevels []logrus.Level
	expected       []logrus.Level
	description    string
}

func TestLevels(t *testing.T) {
	tests := []levelsTest{
		{
			name:           "undefinedAcceptedLevels",
			acceptedLevels: []logrus.Level{},
			expected:       logrus.AllLevels,
			description:    "All logrus levels expected, but did not recieve them",
		},
		{
			name:           "definedAcceptedLevels",
			acceptedLevels: []logrus.Level{logrus.InfoLevel},
			expected:       []logrus.Level{logrus.InfoLevel},
			description:    "Logrus Info level expected, but did not recieve that.",
		},
	}

	for _, test := range tests {
		fn := func(t *testing.T) {
			h.AcceptedLevels = test.acceptedLevels
			levels := h.Levels()
			assert.Equal(t, test.expected, levels, test.description)
		}

		t.Run(test.name, fn)
	}
}

type levelThresholdTest struct {
	name        string
	level       logrus.Level
	expected    []logrus.Level
	description string
}

func TestLevelThreshold(t *testing.T) {
	tests := []levelThresholdTest{
		{
			name:        "unknownLogLevel",
			level:       logrus.Level(100),
			expected:    []logrus.Level{},
			description: "An empty Level slice was expected but was not returned",
		},
		{
			name:        "errorLogLevel",
			level:       logrus.ErrorLevel,
			expected:    []logrus.Level{logrus.PanicLevel, logrus.FatalLevel, logrus.ErrorLevel},
			description: "The panic, fatal, and error levels were expected but were not returned",
		},
	}

	for _, test := range tests {
		fn := func(t *testing.T) {
			levels := LevelThreshold(test.level)
			assert.Equal(t, test.expected, levels, test.description)
		}

		t.Run(test.name, fn)
	}
}

func TestInvalidRegex(t *testing.T) {
	e := &logrus.Entry{}
	h = &Hook{ReplaceList: []string{"\\"}}
	err := h.Fire(e)

	assert.NotNil(t, err)
}

type EntryDataValuesTest struct {
	name        string
	replaceList []string
	logFields   logrus.Fields
	expected    logrus.Fields
	description string
}

// Test that any occurence of a redaction pattern
// in the values of the entry's data fields is redacted.
func TestEntryDataValues(t *testing.T) {
	tests := []EntryDataValuesTest{
		{
			name:        "match on key",
			replaceList: []string{"Password"},
			logFields:   logrus.Fields{"Password": "password123!"},
			expected:    logrus.Fields{"Password": "[MASKED]"},
			description: "Password value should have been redacted, but was not.",
		},
		{
			name:        "string value",
			replaceList: []string{"William"},
			logFields:   logrus.Fields{"Description": "His name is William"},
			expected:    logrus.Fields{"Description": "His name is [MASKED]"},
			description: "William should have been redacted, but was not.",
		},
		{
			name:        "json value",
			replaceList: []string{"(\"primary_account_number\":\")[a-zA-Z0-9=]+(\")", `(track\s*[a-zA-Z0-9_-]*\s*\"\s*:\s*\")\s*[0-9a-zA-Z-_=]+(\")`, "(\"other\":\")[a-zA-Z0-9=]+(\")"},
			logFields:   logrus.Fields{"frame": "{\"primary_account_number\":\"1234567890123456\",\"test\":{\"track2_data\":\"1234\",\"other\":\"aAcd123=\", \"more\":\"ddddd\"}}"},
			expected:    logrus.Fields{"frame": "{\"primary_account_number\":\"[MASKED]\",\"test\":{\"track2_data\":\"[MASKED]\",\"other\":\"[MASKED]\", \"more\":\"ddddd\"}}"},
		},
		{
			name:        "json value and field",
			replaceList: []string{`(primary_account_number\s*[a-zA-Z0-9_-]*\s*\"\s*:\s*\")\s*[0-9a-zA-Z-_=]+(\")`, "(\"track2_data\":\")[a-zA-Z0-9=]+(\")", "(\"other\":\")[a-zA-Z0-9=]+(\")", "pan"},
			logFields:   logrus.Fields{"frame": "{\"primary_account_number\":\"1234567890123456\",\"test\":{\"track2_data\":\"1234\",\"other\":\"aAcd123=\", \"more\":\"ddddd\"}}", "pan": "1234567890123456"},
			expected:    logrus.Fields{"frame": "{\"primary_account_number\":\"[MASKED]\",\"test\":{\"track2_data\":\"[MASKED]\",\"other\":\"[MASKED]\", \"more\":\"ddddd\"}}", "pan": "[MASKED]"},
		},
	}

	for _, test := range tests {
		fn := func(t *testing.T) {
			logEntry := &logrus.Entry{
				Data: test.logFields,
			}
			h = &Hook{ReplaceList: test.replaceList}
			err := h.Fire(logEntry)

			assert.Nil(t, err)
			assert.Equal(t, test.expected, logEntry.Data)
		}
		t.Run(test.name, fn)
	}
}

// Test that any occurence of a redaction pattern
// in the entry's Message field is redacted.
func TestEntryMessage(t *testing.T) {
	logEntry := &logrus.Entry{
		Message: "Secret Password: password123!",
	}
	h = &Hook{ReplaceList: []string{`(Password: ).*`}}
	err := h.Fire(logEntry)

	assert.Nil(t, err)
	assert.Equal(t, "Secret Password: [MASKED]", logEntry.Message)
}

// Logrus fields can have a nil value so test we handle this edge case
func TestNilField(t *testing.T) {
	logEntry := &logrus.Entry{
		Data: logrus.Fields{"Nil": nil},
	}
	h = &Hook{ReplaceList: []string{"foo"}}
	err := h.Fire(logEntry)

	assert.Nil(t, err)
	assert.Equal(t, logrus.Fields{"Nil": nil}, logEntry.Data)
}

type stringerValue struct {
	value string
}

func (v stringerValue) String() string {
	return v.value
}

func TestStringer(t *testing.T) {
	logEntry := &logrus.Entry{
		Data: logrus.Fields{"Stringer": stringerValue{"kind is fmt.Stringer"}},
	}
	h = &Hook{ReplaceList: []string{"kind"}}
	err := h.Fire(logEntry)

	assert.Nil(t, err)
	assert.Equal(t, logrus.Fields{"Stringer": "[MASKED] is fmt.Stringer"}, logEntry.Data)

	var s *stringerValue
	nilStringerEntry := &logrus.Entry{
		Data: logrus.Fields{"Stringer": s},
	}
	err = h.Fire(nilStringerEntry)
	assert.Nil(t, err)

	var stringer fmt.Stringer
	nilStringerEntry = &logrus.Entry{
		Data: logrus.Fields{"Stringer": stringer},
	}
	err = h.Fire(nilStringerEntry)
	assert.Nil(t, err)
}

type TypedString string

// Logrus fields can have re-typed strings so test we handle this edge case
func TestTypedStringValue(t *testing.T) {
	logEntry := &logrus.Entry{
		Data: logrus.Fields{"TypedString": TypedString("kind is string")},
	}
	h = &Hook{ReplaceList: []string{"kind"}}
	err := h.Fire(logEntry)

	assert.Nil(t, err)
	assert.Equal(t, logrus.Fields{"TypedString": "[MASKED] is string"}, logEntry.Data)
}
